This repository is designated to serve as a feed for OpenWrt
(http://openwrt.org) based systems.  As such, the organization, management,
and usage of this repository will be different from the other source
repositories hosted by the AllSeen Alliance.

To use this feed in an OpenWrt build environment, follow these instructions:

1. Copy feeds.conf.default to feeds.conf (if not already done):

cp feeds.conf.default feeds.conf


2. Add the following feeds.conf (replace <openwrt_release> with the
appropriate branch that corresponds to the OpenWrt release you are building):

src-git alljoyn git://git.allseenalliance.org/gerrit/core/openwrt_feed;<openwrt_release>
src-git alljoyn-gwagent git://git.allseenalliance.org/gerrit/gateway/gwagent_openwrt_feed;<openwrt_release>

# For Barrier Breaker
src-git alljoyn git://git.allseenalliance.org/gerrit/core/openwrt_feed;barrier_breaker
src-git alljoyn-gwagent git://git.allseenalliance.org/gerrit/gateway/gwagent_openwrt_feed;barrier_breaker

# For Chaos Calmer
src-git alljoyn git://git.allseenalliance.org/gerrit/core/openwrt_feed;chaos_calmer
src-git alljoyn-gwagent git://git.allseenalliance.org/gerrit/gateway/gwagent_openwrt_feed;chaos_calmer


3. Update the feed information:

./scripts/feeds update -a


4. Add the the packages from the AllJoyn feed to build system:

./scripts feeds install -a -p alljoyn
./scripts feeds install -a -p alljoyn-gwagent

5. Enable AllJoyn Gateway Agent in the build:

make menuconfig

	Networking --->
		< > alljoyn-gwagent --->
			< > alljoyn-gwagent-samples

